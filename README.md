# block

Package block implements 4 signals to listen to and block until they're called. This is used for code that doesn't hang and is made to run indefinitely as a service until it fails.

### Usage

```go
package main

import "gitlab.com/diamondburned/block"

func main() {
	// code here

	// This will wait until a signal is received to end the application
	block.Wait()
}
```
