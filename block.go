// Package block implements 4 signals to listen to and block until
// they're called. This is used for code that doesn't hang and is
// made to run indefinitely as a service until it fails.
package block

import (
	"os"
	"os/signal"
	"syscall"
)

// Wait for a signal to end
func Wait() {
	<-Ch()
}

// Ch returns the channel for blocking
func Ch() chan os.Signal {
	sc := make(chan os.Signal)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	return sc
}
